# Changelog

## [v1.12.0] - 2024-09-19

- Fixed `acl.getPricipal()` as `acl.getPrincipal()`
- Moved to maven-portal-bom 4.0.0{-SNAPSHOT}

## [v1.11.0]  - 2021-02-02

### Features

- Updated the set administrator method for VRE folders [#19952]



## [v1.10.1] - 2019-12-19

### Features

- Updated to Git and Jenkins



## [v1.10.0] - 2019-07-30

### Features

- Added support for users in different Gateway [#17080]
- Updated the list of shared classes with the tree widget



## [v1.9.0] - 2019-06-05

### Features

- Updated to StorageHub [#13226]



## [v1.8.2] - 2017-02-24

### Features

- Stylesheet fix [#3236]



## [v1.8.1] - 2016-11-25

### Features

- Removed ASL Session dependency



## [v1.7.0] - 2016-05-31

### Features

- Migration to Liferay 6.2 [#4128]



## [v1.6.0] - 2016-04-14

### Features

- Enhancement in order to display short VREs name [#3277]



## [v1.5.1] - 2016-01-04

### Features

- Removed portal-framework dependecy from Etics



## [v1.5.0] - 2015-09-28

### Features

- Added code to retrieve (only) the users belonging to the organization read from (asl) session group [#401]



## [v1.4.0] - 2015-06-04

### Features

- Porting to HL 2.0 [#211]



## [v1.3.0] - 2015-04-15

### Features

- Added Edit Permission for Contacts



## [v1.2.0] - 2014-24-01

### Features

- Added method get administrator by folder id
- Added class SimpleMultiDragWorkspaceContact to realize "Edit Administrators"



## [v1.1.0] - 2014-06-27

### Features

- Integrated sharing to groups [#2897]
- Sharing Panel usability improved [#2798]



## [v1.0.2] - 2014-06-04

### Features

- Updated pom to support new portal configuration



## [v1.0.1] - 2014-04-03

### Features

- Commented the methods of Notification Producer



## [v1.0.0] - 2014-04-03

### Features

- First release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
