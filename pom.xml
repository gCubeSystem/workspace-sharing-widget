<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>org.gcube.tools</groupId>
		<version>1.2.0</version>
		<relativePath />
	</parent>

	<!-- POM file generated with GWT webAppCreator -->
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.gcube.portlets.widgets</groupId>
	<artifactId>workspace-sharing-widget</artifactId>
	<packaging>jar</packaging>
	<version>1.12.0</version>
	<name>gCube Workspace Sharing Widget</name>
	<description>
		Workspace Sharing Widget is a widget that allows the sharing of items present in the D4Science Workspace.
	</description>

	<scm>
		<connection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</connection>
		<developerConnection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</developerConnection>
		<url>https://code-repo.d4science.org/gCubeSystem/${project.artifactId}</url>
	</scm>
	<properties>
		<!-- Convenience property to set the GWT version -->
		<gwtVersion>2.6.1</gwtVersion>
		<gxt2Version>2.6.1</gxt2Version>

		<distroDirectory>distro</distroDirectory>
		<!-- GWT needs at least java 1.6 -->
		<maven.compiler.source>1.7</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>

		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

	<!-- Profiles required for managing SHUB vesion via maven-portal-bom -->
	<profiles>
		<!-- PROD profile -->
		<profile>
			<id>workspace-sharing-widget-release-profile</id>
			<activation>
				<property>
					<name>Release</name>
				</property>
			</activation>
			<properties>
				<maven-portal-bom>4.0.0</maven-portal-bom>
			</properties>
		</profile>
		<!-- DEV profile -->
		<profile>
			<id>workspace-sharing-widget-snapshot-profile</id>
			<activation>
				<property>
					<name>!Release</name>
				</property>
			</activation>
			<properties>
				<maven-portal-bom>4.0.0-SNAPSHOT</maven-portal-bom>
			</properties>
		</profile>
	</profiles>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.gcube.distribution</groupId>
				<artifactId>maven-portal-bom</artifactId>
				<version>${maven-portal-bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>


	<dependencies>
		<!-- This dependency is needed to use GWT UI BInder without old Xerces 
			version of gCore complaining -->
		<dependency>
			<groupId>xerces</groupId>
			<artifactId>xercesImpl</artifactId>
			<version>2.9.1</version>
			<scope>provided</scope>
		</dependency>
		<!-- Google Web Toolkit (GWT) -->
		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-user</artifactId>
			<version>${gwtVersion}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.sencha.gxt</groupId>
			<artifactId>gxt2.2.5-gwt2.X</artifactId>
			<version>${gxt2Version}</version>
			<scope>compile</scope>
		</dependency>
		<!-- PORTAL MANAGER -->
		<dependency>
			<groupId>org.gcube.common.portal</groupId>
			<artifactId>portal-manager</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>storagehub-model</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- FWS -->
		<dependency>
			<groupId>org.gcube.resources.discovery</groupId>
			<artifactId>ic-client</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-scope-maps</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- END FWS -->
		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-encryption</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.gcube.dvos</groupId>
			<artifactId>usermanagement-core</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.gcube.portlets.user</groupId>
			<artifactId>gcube-widgets</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- StorageHub -->
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>storagehub-client-library</artifactId>
			<scope>compile</scope>
		</dependency>

		<!-- LOGGER -->
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
		</dependency>

		<dependency>
			<groupId>com.liferay.portal</groupId>
			<artifactId>portal-service</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.gcube.portal</groupId>
			<artifactId>social-networking-library</artifactId>
			<version>[1.0.0, 2.0.0-SNAPSHOT)</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.gcube.applicationsupportlayer</groupId>
			<artifactId>aslsocial</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- JSON PARSER -->
		<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>20090211</version>
		</dependency>

	</dependencies>
	<build>
		<resources>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>${maven.compiler.source}</source>
					<target>${maven.compiler.target}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<skipTests>true</skipTests>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>descriptor.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>servicearchive</id>
						<phase>install</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			
			<!-- GWT Maven Plugin - not needed -->
			<!-- GWT Maven Plugin -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>gwt-maven-plugin</artifactId>
				<version>${gwtVersion}</version>
				<executions>
					<execution>
						<goals>
							<goal>compile</goal>
							<!-- <goal>test</goal> -->
						</goals>
					</execution>
				</executions>
				<configuration>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
